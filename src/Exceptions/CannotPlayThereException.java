package Exceptions;

public abstract class CannotPlayThereException extends RuntimeException
{
    public CannotPlayThereException(String message)
    {
        super(message);
    }
}
