package Exceptions;

public class CaseAlreadyUsedException extends CannotPlayThereException
{
    public CaseAlreadyUsedException()
    {
        super("The place had already been played");
    }
}
