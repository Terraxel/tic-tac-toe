package Exceptions;

public class GameIsFinishedException extends CannotPlayThereException
{
    public GameIsFinishedException()
    {
        super("You cannot play, the game is finished");
    }
}
