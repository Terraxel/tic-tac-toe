package Views;

import Model.Case;
import Model.Game;
import Model.Grid;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class GameView
{
    private static final int nbButtons = 25;
    private static final int buttonDimension = 35;
    private static final int upperPanelHeight = 100;

    private final GameViewListener listener;

    Button[][] buttons = new Button[nbButtons][nbButtons];
    Label statusLabel;

    public GameView(Stage primaryStage, GameViewListener gameViewListener)
    {
        this.listener = gameViewListener;

        primaryStage.setTitle("OXO");
        AnchorPane ap = SetWindow((buttonDimension * nbButtons) + upperPanelHeight, buttonDimension * nbButtons);

        Scene scene = new Scene(ap, (buttonDimension * nbButtons), (buttonDimension * nbButtons) + upperPanelHeight);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void DisplayErrorMessage(String message)
    {
        System.out.println(String.format("Error : %s", message));
    }

    public void Update(Game game)
    {
        this.UpdateCases(game.GetGrid().GetCases());
        this.UpdateLabel(game.GetCurrentPlayer(), game.GetWinner());
    }

    private void UpdateLabel(String currentPlayer, String winner)
    {
        if(winner != null)
        {
            this.statusLabel.setText(String.format("Player %s won !", winner));
        }
        else
        {
            this.statusLabel.setText(String.format("Player %s has to play !", currentPlayer));
        }
    }

    private void UpdateCases(Case[][] cases)
    {
        for(int i = 0; i < nbButtons; i++)
        {
            for(int j = 0; j < nbButtons; j++)
            {
                this.buttons[i][j].setText(cases[i][j].GetContent());
            }
        }
    }

    private AnchorPane SetWindow(double height, double width)
    {
        AnchorPane mainPane = new AnchorPane();

        AnchorPane upperPane = new AnchorPane();
        upperPane.setPrefHeight(upperPanelHeight);
        upperPane.getChildren().add(NewGameButton());

        this.statusLabel = CurrentPlayerLabel();
        upperPane.getChildren().add(this.statusLabel);

        AnchorPane gamePane = ButtonGrid((height - upperPanelHeight), width);

        mainPane.getChildren().add(upperPane);
        mainPane.getChildren().add(gamePane);
        return mainPane;
    }

    private Button NewGameButton()
    {
        Button b = new Button("New game");
        b.setLayoutX(15);
        b.setLayoutY(20);

        b.setOnMouseClicked(e -> listener.OnNewGameClick());

        return b;
    }

    private static Label CurrentPlayerLabel()
    {
        Label l = new Label("Current player : X");
        l.setLayoutX(200);
        l.setLayoutY(25);

        return l;
    }

    private AnchorPane ButtonGrid(double height, double width)
    {
        System.out.println(width + " " + height);

        AnchorPane pane = new AnchorPane();
        pane.setLayoutY(upperPanelHeight);
        pane.setPrefHeight(height);
        pane.setPrefWidth(width);

        for(int row = 0; row < nbButtons; row++)
        {
            for(int col = 0; col < nbButtons; col++)
            {
                Button button = CreateGameButton(col, row);
                this.buttons[row][col] = button;
                pane.getChildren().add(button);
            }
        }

        return pane;
    }

    private Button CreateGameButton(int currentColumn, int currentRow)
    {
        Button button = new Button();
        button.setPrefWidth(buttonDimension);
        button.setPrefHeight(buttonDimension);
        button.setLayoutX(currentColumn * buttonDimension);
        button.setLayoutY(currentRow * buttonDimension);

        button.setOnMouseEntered(e -> {
            Button b = (Button)e.getSource();
            b.setStyle("-fx-background-opacity: 0.5");
        });

        button.setOnMouseExited(e->{
            Button b = (Button)e.getSource();
            b.setStyle("-fx-background-opacity: 1");
        });

        button.setOnMouseClicked(e -> {
            Button b = (Button)e.getSource();

            for(int i = 0; i < nbButtons; i++)
            {
                for(int j = 0; j < nbButtons; j++)
                {
                    if(b.equals(this.buttons[j][i]))
                    {
                        listener.OnButtonClick(j, i);
                        break;
                    }
                }
            }
        });

        return button;
    }
}
