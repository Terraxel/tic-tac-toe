package Views;

public interface GameViewListener
{
    void OnButtonClick(int x, int y);
    void OnNewGameClick();
}
