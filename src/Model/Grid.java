package Model;

import Controllers.GameController;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Grid
{
    private Case[][] cases;
    private int nbCases;

    public Grid(int nbCases)
    {
        this.nbCases = nbCases;
        this.cases = new Case[nbCases][nbCases];

        for(int i = 0; i < nbCases; i++)
        {
            for(int j = 0; j < nbCases; j++)
            {
                this.cases[i][j] = new Case();
            }
        }
    }

    public void PlayAtPosition(int x, int y, String charToPlay)
    {
        this.cases[x][y].Play(charToPlay);
    }

    public void Clear()
    {
        for(int i = 0; i < nbCases; i++)
        {
            for(int j = 0; j < nbCases; j++)
            {
                this.cases[i][j].Reset();
            }
        }
    }

    public Case[][] GetCases()
    {
        return this.cases;
    }

    public String VerifyVictory()
    {
        return null;
    }
}
