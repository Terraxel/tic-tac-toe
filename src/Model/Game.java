package Model;

public class Game
{
    private Grid grid;
    private boolean playerXPlaying;

    public Game(int nbButtons)
    {
        this.grid = new Grid(nbButtons);
        this.playerXPlaying = true;
    }

    public void Play(int x, int y)
    {
        String characterToPlay = GetCurrentPlayer();

        this.grid.PlayAtPosition(x, y, characterToPlay);
        //Verif
        this.FinishTurn();
    }

    public void NewGame()
    {
        this.grid.Clear();
        this.playerXPlaying = true; //X player always begins
    }

    public Grid GetGrid()
    {
        return this.grid;
    }

    public String GetCurrentPlayer()
    {
        return playerXPlaying ? "X" : "O";
    }

    public String GetWinner()
    {
        return null;
    }

    private void FinishTurn()
    {
        this.playerXPlaying = !playerXPlaying;
    }

    private void VerifyVictory(int x, int y)
    {
        
    }
}
