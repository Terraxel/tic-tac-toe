package Model;

import Exceptions.CannotPlayThereException;
import Exceptions.CaseAlreadyUsedException;

public class Case
{
    private String characterPlayed;
    private boolean alreadyPlayed;

    public Case()
    {
        this.characterPlayed = null;
        this.alreadyPlayed = false;
    }

    public void Play(String characterToPlay)
    {
        if(alreadyPlayed)
            throw new CaseAlreadyUsedException();
        else
        {
            this.characterPlayed = characterToPlay;
            this.alreadyPlayed = true;
        }
    }

    public String GetContent()
    {
        return this.characterPlayed;
    }

    public void Reset()
    {
        this.characterPlayed = null;
        this.alreadyPlayed = false;
    }
}
