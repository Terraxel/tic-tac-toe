package sample;

import Controllers.GameController;
import Views.GameView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application
{
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        GameController controller = new GameController(primaryStage);
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
