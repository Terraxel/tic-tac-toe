package Controllers;

import Exceptions.CannotPlayThereException;
import Exceptions.GameIsFinishedException;
import Model.Game;
import Views.GameView;
import Views.GameViewListener;
import javafx.stage.Stage;

public class GameController implements GameViewListener
{
    public static final int nbButtons = 25;

    GameView view;
    Game game;

    public GameController(Stage primaryStage)
    {
        view = new GameView(primaryStage, this);
        game = new Game(nbButtons);
    }

    @Override
    public void OnButtonClick(int x, int y)
    {
        try
        {
            game.Play(x, y);
            view.Update(game);
        }
        catch (CannotPlayThereException ex)
        {
            view.DisplayErrorMessage(ex.getMessage());
        }
    }

    @Override
    public void OnNewGameClick()
    {
        this.game.NewGame();
        view.Update(game);
    }
}
